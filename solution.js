function processData(input) {
    var matrix = input.split("\n"),
        rows = parseInt(matrix[0]),
        cols = parseInt(matrix[1]),
        biggestRegion = 0;
    matrix = matrix.slice(2, matrix.length);

    for(var c = 0; c < matrix.length; c++){
        matrix[c] = matrix[c].split(' ');
    }

    for(var i = 0; i < rows; i++){
        for(var j = 0; j < cols; j++){
            if(matrix[i][j] == '1'){
                var regionSize = findRegionSize(i, j, rows, cols, matrix);

                if(regionSize > biggestRegion){
                    biggestRegion = regionSize;
                }
            }
        }
    }

    console.log(biggestRegion);
}

function findRegionSize(i, j, rows, cols, matrix){
    if(i < 0 ||
       j < 0 ||
       i >= rows ||
       j >= cols){
        return 0;
    }
    if(matrix[i][j] != '1'){
        return 0;
    }

    matrix[i][j] = -1;

    var adjacents = 1;

    for(var a = i-1; a <= i+1; a++){
      for(var b = j-1; b <= j+1; b++){
        adjacents += findRegionSize(a, b, rows, cols, matrix);
      }
    }

    return adjacents;
}

process.stdin.resume();
process.stdin.setEncoding("ascii");
_input = "";
process.stdin.on("data", function (input) {
    _input += input;
});

process.stdin.on("end", function () {
   processData(_input);
});
